#About
This repository contains IPlan described in.

#Dependencies

##G++4.8

The code is tested only with g++4.8.

Get Gurobi Optimization 6.5.0 from Grubi Optimization.
Configure the environmental variables as below.
You also need to obtain a gurobi license file.

##Gurobi Optimization 6.5.0

```.bashrc
export GUROBI_HOME="$HOME/gurobi650/linux64"
export PATH="${PATH}:${GUROBI_HOME}/bin"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
```

##Gurobi Python Module

```
cd $(HOME)/gurobi650/linux64; python setup.py install
```

## psutils

```
pip3 install psutils
```
