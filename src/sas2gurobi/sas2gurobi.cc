#include <unistd.h>
#include <iostream>
#include <cassert>
#include <limits.h>
#include <chrono>
#include "sas.h"
#include "pgraph.h"
#include "model.h"
#include "config.h"
#include "measure.h"
#include "config.h"
#include "gurobi_generator.h"
#include "gurobi_c++.h"
using namespace std;

int
main(int argc, char **argv) {
	Config config = parse_config(argc,argv);


	/* XXX: (zinc) time-wasting, it is enough to parse once, gen_pg once */
	start_timer();
	Problem *prob = new Problem(config.ifname);
	stop_timer("parse");

	/* XXX: (zinc) time-wasting, it is enough to parse once, to gen_pg once */
	SimplePG pg;
	Depth pg_depth = 1;
	if (config.use_simple_pg) {
		start_timer();
		pg_depth = simple_extract_pg(prob, pg);
		stop_timer("pg_extract");

		cout << "lower_bound of depth obtained from pg: " << pg_depth << endl;
	}

	int r1;
	do {
		chrono::time_point<chrono::system_clock> start_time = chrono::system_clock::now();

		std::cout << "now at depth: " << pg_depth << std::endl;
		r1 = state_change_variable(pg_depth, prob,config, pg);

		chrono::time_point<chrono::system_clock> end_time = chrono::system_clock::now();
		auto timespan = end_time - start_time;
		cout << "depth:" << pg_depth << " " 
			 << chrono::duration_cast<chrono::milliseconds>(timespan).count()
	     << "[ms]" << endl;
		++pg_depth;
	} while (r1 != GRB_OPTIMAL);

	cout << "Solution Found!" << endl;
	cout << "solution_depth:" << pg_depth-1 << endl;

	delete prob;
	config.dump();
}
