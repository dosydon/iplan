#include "sas.h"

#include <cassert>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <lemon/list_graph.h>
#include <lemon/connectivity.h>
#include <lemon/core.h>

using std::istream;
using std::ifstream;
using std::cerr;
using std::endl;
using std::cout;
using std::sort;
using std::max;

using namespace lemon;

static inline int
toInt(const string &s) {
	return stoi(s, nullptr);
}
static inline int
get_next_int(istream &is) {
	string tmp;
	is >> tmp;
	return toInt(tmp);
}
static inline void
validate_next(istream &is, const string &valid) {
	string in;
	is >> in;
	assert(in == valid);
}

Problem::Problem(const string &sas_fname):max_axiom_layer(-1){
	parse(sas_fname);
	preprocess();
	if(n_axioms > 0)
		init_scc();
	cout << "max_axiom_layer:" << max_axiom_layer << endl;
}

Problem::~Problem(){};

void
Problem::preprocess(){
	init_aset_dict();
}
void
Problem::parse(const string &sas_fname){
	ifstream ifs(sas_fname);
	if (ifs.fail()) {
		cerr << "failure: ifstream couldn't open" << endl;
		exit(EXIT_FAILURE);
	}

	// version section
	validate_next(ifs, "begin_version");
	version = get_next_int(ifs);
	validate_next(ifs, "end_version");

	// metric section
	validate_next(ifs, "begin_metric");
	metric = (get_next_int(ifs) == 1);
	validate_next(ifs, "end_metric");

	// variable section
	n_vars = get_next_int(ifs);
	for (Var i = 0; i < n_vars; ++i) {
		variable v; validate_next(ifs, "begin_variable");
		ifs >> v.name;
		v.axiom_layer= get_next_int(ifs);
		max_axiom_layer = max(v.axiom_layer,max_axiom_layer);
		v.range = get_next_int(ifs);
		ifs.ignore();

		for (Val j = 0; j < v.range; ++j) {
			string tmp;
			getline(ifs, tmp);
			v.atoms.push_back(tmp);
		}
		validate_next(ifs, "end_variable");

		vars.push_back(v);
	}

	// mutex proup section
	unsigned int n_mtxs = get_next_int(ifs);

	for (unsigned int i = 0; i < n_mtxs; ++i) {
		mutex_group mtx;
		validate_next(ifs, "begin_mutex_group");
		mtx.n = get_next_int(ifs);
		for (unsigned int i = 0; i < mtx.n; ++i) {
			Prop ip;
			ifs >> ip.first >> ip.second;
			mtx.list.push_back(ip);
		}
		mtxs.push_back(mtx);
		validate_next(ifs, "end_mutex_group");
	}

	// initial state section
	validate_next(ifs, "begin_state");
	for (Var i = 0; i < n_vars; ++i) {
		Val val = get_next_int(ifs);
		init.push_back(val);
	}
	validate_next(ifs, "end_state");

	// goal state section
	validate_next(ifs, "begin_goal");
	unsigned n = get_next_int(ifs);
	for (unsigned int i = 0; i < n; ++i) {
		Prop ip;
		ifs >> ip.first >> ip.second;
		goal.push_back(ip);
	}
	validate_next(ifs, "end_goal");

	// operator section
	n_ops = get_next_int(ifs);

	for (AI i = 0; i < n_ops; ++i) {
		op ope;
		validate_next(ifs, "begin_operator");
		ifs.ignore();
		getline(ifs, ope.name);
		// name may hold space (ex. action ope ope ...)
		ope.n_prevailCond = get_next_int(ifs);
		for (unsigned int i = 0; i < ope.n_prevailCond; ++i) {
			Prop ip;
			ifs >> ip.first >> ip.second;
			ope.prevailConditions.push_back(ip);
		}
		ope.n_effects = get_next_int(ifs);
		for (unsigned int i = 0; i < ope.n_effects; ++i) {
			effect ef;
			ef.n_assoc_conditions = get_next_int(ifs);
			for (unsigned int i = 0; i < ef.n_assoc_conditions; ++i) {
				Prop ip;
				ifs >> ip.first >> ip.second;
				ef.assoc_condition.push_back(ip);
			}
			ef.var = get_next_int(ifs);
			auto val = get_next_int(ifs);
			ef.preval =
			    val == -1 ? NOT_REQUIRED : static_cast<unsigned int>(val);
			ef.postval = get_next_int(ifs);

			ope.effects.push_back(ef);
		}
		ope.cost = get_next_int(ifs);
		validate_next(ifs, "end_operator");
		operators.push_back(ope);
	}

	n_axioms = get_next_int(ifs);
	for (unsigned int i = 0; i < n_axioms; ++i) {
		axiom ax;
		validate_next(ifs, "begin_rule");
		ifs.ignore();
		ax.n_conditions = get_next_int(ifs);
		for (unsigned int i = 0; i < ax.n_conditions; ++i) {
			Prop ip;
			ifs >> ip.first >> ip.second;
			ax.conditions.push_back(ip);
		}
		Prop ip;unsigned int dummy;
		ifs >> ip.first >> dummy >> ip.second;
		ax.effect = ip;
		validate_next(ifs, "end_rule");
		axioms.push_back(ax);
	}
}

void
Problem::init_aset_dict() {
// 	auto operators = prob->operators;
// 	auto vars = prob->vars;
	for (Var i = 0; i < n_vars; ++i)
		for (Val j = 0; j < vars[i].range; ++j) {
			Prop p(i, j);
			addf[p] = ASet();
			pref[p] = ASet();
			delf[p] = ASet();
		}

	int opi = 0;
	for (auto op : operators) {
		for (auto ef : op.effects) {
			auto var = ef.var;
			auto pre = ef.preval;
			auto post = ef.postval;
			if (pre == NOT_REQUIRED) {
				Prop p(var, post);
				addf[p].insert(opi);
				for (Val val = 0; val < vars[var].range; ++val)
					if (post != val) {
						Prop p(var, val);
						delf[p].insert(opi);
					}
			} else {
				Prop p1(var, pre);
				pref[p1].insert(opi);

				if (pre != post)
					delf[p1].insert(opi);

				Prop p2(var, post);
				addf[p2].insert(opi);
			}
		}

		for (auto p : op.prevailConditions) {
			pref[p].insert(opi);
			addf[p].insert(opi);
		}

		++opi;
	}
}

void Problem::init_scc()
{
	ListDigraph G;
	ListDigraph::NodeMap<int> CON(G);
	map<Prop,ListDigraph::Node> M;
	set<PropPair> S;

	for (Var i = 0; i < n_vars; ++i){
		for (Val j = 0; j < vars[i].range; ++j) {
			Prop p(i, j);
			M[p] = G.addNode();
			if(vars[i].is_axiom()) {
				lnt[p] = ASet();
				def[p] = ASet();
				ext[p] = ASet();
			}
		}
	}

	for(auto axiom : axioms){
		auto ef = axiom.effect;
		for(auto prop:axiom.conditions){
			if(!(vars[prop.first].is_axiom() && prop.second == 0)){
				set<PropPair>::iterator it = S.find(PropPair(prop,ef));
				if(it == S.end()){
					S.insert(PropPair(prop,ef));
					G.addArc(M[prop],M[ef]);
				}
			}
		}
	}
// 	for(ListDigraph::ArcIt i(G); i!=INVALID; ++i){
// 		cout << G.id(G.source(i)) << " -> " << G.id(G.target(i)) << endl;
// 	}

	stronglyConnectedComponents(G,CON);
	for(auto item:M){
		auto prop = item.first;
		auto node = item.second;
		scc[prop] = vector<Prop>();

		for(auto inner_item:M){
			auto inner_prop = inner_item.first;
			auto inner_node = inner_item.second;
			if(CON[node] == CON[inner_node]){
				scc[prop].push_back(inner_prop);
			}
		}
		sort(scc[prop].begin(),scc[prop].end());
	}

	int axi = 0;
	for(auto axiom : axioms){
		auto ef = axiom.effect;
		def[ef].insert(axi);
		vector<Prop> intersection;
		set_intersection(axiom.conditions.begin(),axiom.conditions.end(),
				scc[ef].begin(),scc[ef].end(),back_inserter(intersection));
		if(intersection.size() > 0){
			lnt[ef].insert(axi);
		}else{
			ext[ef].insert(axi);
		}
		++axi;
	}
}

int
calc_min_cost(Problem *prob) {
	int min_cost = INT_MAX;

	/* NOTE: min_cost can be calculated statically (e.g. while building prob) */
	if (prob->metric)
		for (AI i = 0; i < prob->n_ops; ++i)
			min_cost =
			    (min_cost < prob->operators[i].cost ? min_cost
			                                        : prob->operators[i].cost);
	else
		min_cost = 1;

	return min_cost;
}
