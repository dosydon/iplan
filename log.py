class Tag:
    HP=1
    ENCODE=2
    DECODE=3
    TRANSLATE=4
    PREPROCESS=5
    VAL=6
    MZN=7
    CLINGO=8
    GRINGO=9
    CLASP=10
    MINION=11

class Result:
    def __init__(self,cmd,out,start,end,tag,retcode):
        self.cmd = cmd
        self.out = out
        self.start = start
        self.end = end
        self.tag = tag
        self.retcode=retcode
    def __repr__(self):
        return "{}: {:f} {}".format(self.cmd,self.end-self.start,self.tag,self.retcode)

def verbose_log(log):
    for item in log:
        print(item)

def aggregate_log(log):
    attrs = [attr for attr in vars(Tag) if not attr.startswith("__")]
    for attr in attrs:
        total = sum([0.0] + [item.end-item.start for item in log if item.tag == getattr(Tag,attr)])
        print("{:10s}:{}".format(attr,total))


