#pragma once

#include "model.h"
#include "limits.h"
#include <map>

using std::map;

#define PG_UNACTIVATED (UINT_MAX)
using SimplePG = map<Prop, Depth>;

Depth simple_extract_pg(const Problem *prob, SimplePG &pg);
