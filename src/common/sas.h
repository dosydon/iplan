#pragma once
#include <string>
#include <vector>
#include <limits.h>
#include <map>
#include <set>
#define NOT_REQUIRED (UINT_MAX)

using std::string;
using std::vector;
using std::pair;
using std::map;
using std::set;

using Var = unsigned int;
using Val = unsigned int;
using Prop = pair<Var, Val>;
typedef pair<Prop,Prop> PropPair;
using AI = unsigned int; /* action_index */
using ASet = set<AI>;
using ASetDict = map<Prop, ASet>;

struct variable {
	string name;
	int axiom_layer;
	unsigned int range;
	vector<string> atoms;
	bool is_axiom() const {return axiom_layer >= 0;};
	bool is_essential()const {return axiom_layer == -1;};
};

struct mutex_group {
	unsigned int n;
	vector<Prop> list;
};

struct effect {
	unsigned int n_assoc_conditions;
	std::vector<Prop> assoc_condition;
	Var var;
	Val preval;
	Val postval;
};

struct op {
	string name;
	unsigned int n_prevailCond;
	vector<Prop> prevailConditions;
	unsigned int n_effects;
	vector<effect> effects;
	int cost;  // optional
};

struct axiom {
// 	unsigned int n_axioms;
	unsigned int n_conditions;
	vector<Prop> conditions;
	Prop effect;
};

class Problem {
	public:
		unsigned int version, n_vars, n_ops, n_mtxs,n_axioms;
		int max_axiom_layer;
		bool metric;
		vector<variable> vars;
		vector<mutex_group> mtxs;
		vector<Val> init;
		vector<Prop> goal;
		vector<op> operators;
		vector<axiom> axioms;
		ASetDict addf, delf, pref;
		ASetDict def,ext,lnt;
		map<Prop,vector<Prop> >scc;

		Problem(const string &sas_fname);
		~Problem();
	private:
		void parse(const string &sas_fname);
		void preprocess();
		void init_aset_dict();
		void init_scc();
};

int calc_min_cost(Problem *prob);
