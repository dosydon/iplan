#pragma once
#include "pgraph.h"
#include "model.h"
#include "config.h"
int state_change_variable(const Depth depth, const Problem *prob, const Config config, SimplePG &pg);
