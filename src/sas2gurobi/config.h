#pragma once
#include <unistd.h>
#include <iostream>
using OptimizationLevel = enum { Satisficing= 0, StepOptimal, GlobalOptimal};
using ParallelLevel = enum { Sequential= 0, AllStep};

typedef struct{
	int depth;
	bool use_among,use_simple_pg,use_branch_priority,use_hint;
	OptimizationLevel opt_level;
	ParallelLevel para_level;
	std::string ifname,ofname;
	void dump() const{
		std::cout << "depth:" << depth << std::endl;
		std::cout << "branch_priority:" << use_branch_priority << std::endl;
		std::cout << "among:" << use_among<< std::endl;
		std::cout << "ifname:" << ifname<< std::endl;
	}
} Config;

Config parse_config(int argc, char **argv);
