import time
import utils
import subprocess
import os
import signal
from log import Result
from command import Command


class Executor:
    def __init__(self,log,global_timeout=None):
        self.log = log
        self.global_timeout=global_timeout
    def run(self,cmd):
        proc = cmd.proc(preexec_fn=os.setsid)

        out = None
        err=None
        start = time.time()
        try:
            out,err=proc.communicate()
        except (Exception,KeyboardInterrupt) as e:
            os.killpg(proc.pid,signal.SIGTERM)
            print("killing {}".format(proc.pid))
            proc.kill()
            out,err=proc.communicate()
            raise e
        finally:
            end = time.time()

            if out:
                out = out.decode("utf-8")
                print(out)
            if err:
                print(err)

            res = Result(cmd,out,start,end,cmd.tag,proc.returncode)
            self.log.append(res)
        return res

if __name__ == '__main__':
    ex = Executor()
    ex.run(Command("ls","tag"))


