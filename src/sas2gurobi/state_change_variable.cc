#include <iostream>
#include <map>
#include <set>
#include <utility>
#include <algorithm>
#include <cassert>

#include "gurobi_c++.h"
#include "measure.h"
#include "gurobi_var.h"
#include "gurobi_generator.h"
using namespace std;

int
state_change_variable(const Depth depth, const Problem *prob,const Config config, SimplePG &pg) {
	GRBEnv env;
	GRBModel model = GRBModel(env);
	GrbLevelEnv level_env;
	GrbLevelSat level_sat;
	GrbLevelActions level_actions;
	GrbLevelRanks level_ranks;
	GrbLevelAxioms level_axioms;
	GrbLevelSS level_ss;
	GrbLevelGTS level_gts;

	map<Prop, ASet *> add_sub_pref, del_sub_pref, pre_sub_delf, pre_prod_delf;

	cerr << "LOG: gurobi cap solver starts" << endl
	     << "LOG: depth=" << depth << endl;

	try {
		auto n_vars = prob->n_vars;
		auto n_ops = prob->n_ops;
		auto operators = prob->operators;
		auto vars = prob->vars;
		auto n_axioms = prob->n_axioms;


		level_env.resize(depth + 1);
		for (Depth t = 0; t <= depth; ++t)
			for (Var i = 0; i < n_vars; ++i)
				if(!vars[i].is_axiom())
					for (Val j = 0; j < vars[i].range; ++j) {
						Prop p(i, j);
						string prop_name = vars[i].name + "_j" + to_string(j) +
										   "_t" + to_string(t);
						level_env[t][p] = create_scv(model, prop_name);
					}

		level_sat.resize(depth + 1);
		for (Depth t = 0; t <= depth; ++t)
			for (Var i = 0; i < n_vars; ++i)
				for (Val j = 0; j < vars[i].range; ++j) {
					Prop p(i, j);
					string prop_name = "sat_" + vars[i].name + "_j" +
					                   to_string(j) + "_t" + to_string(t);
					level_sat[t][p] = ADDBINARY(model, prop_name);
				}

		level_actions.resize(depth);
		for (Depth t = 0; t < depth; ++t) {
			level_actions[t].resize(n_ops);
			for (Var i = 0; i < n_ops; ++i)
				level_actions[t][i] =
				    ADDBINARY_WITH_COST(model, operators[i].name,
				                        prob->metric ? operators[i].cost : 1.0);
		}

		if(n_axioms > 0){
			level_ranks.resize(depth + 1);
			level_gts.resize(depth+1);
			for (Depth t = 0; t <= depth; ++t){
				for (Var i = 0; i < n_vars; ++i){
					if(vars[i].is_axiom()){
						Prop p =Prop(i,1);
						string rank_name = "rank_" + vars[i].name + "_t" + to_string(t);
						auto grb = model.addVar(0.0,1.0*n_vars*2,0.0,GRB_INTEGER,rank_name);
						level_ranks[t][i] = grb;
						for(auto index : prob->lnt.at(p)){
							auto axiom = prob->axioms.at(index);
							vector<Prop> is;
							set_intersection(axiom.conditions.begin(),axiom.conditions.end(),
									prob->scc.at(p).begin(),prob->scc.at(p).end(),back_inserter(is));
							for(auto b : is){
								pair<Var,Var> q = pair<Var,Var>(p.first,b.first);
								level_gts[t][q] = ADDBINARY(model,"gt_" 
										"("+to_string(p.first) + "," + to_string(p.second) + ")"+
										"("+to_string(b.first) + "," + to_string(b.second) + ")"+
										"_t"+to_string(t));
							}
						}
					}
				}
			}

			level_axioms.resize(depth+1);
			level_ss.resize(depth+1);
			for (Depth t = 0; t <= depth; ++t) {
				level_axioms[t].resize(n_axioms);
				level_ss[t].resize(n_axioms);
				for (unsigned int i = 0; i < n_axioms; ++i){
					level_axioms[t][i] = ADDBINARY(model, "axiom_" +to_string(i) + "_t" + to_string(t));
					level_ss[t][i] = ADDBINARY(model,"s_"+to_string(i)+"_t"+to_string(t));
				}
			}
		}

		model.set(GRB_IntAttr_ModelSense, 1);  // MINIMIZE
		model.getEnv().set(GRB_IntParam_Threads,1);

		model.update();

		/* NOTE: objective function must be built after adding action vars */

		GRBLinExpr obj;
		switch (config.opt_level) {
			case Satisficing:
				obj = 0;
				break;
			case StepOptimal:
			case GlobalOptimal:
				for (Depth t = 0; t < depth; ++t)
					for (Var i = 0; i < n_ops; ++i)
						obj += operators[i].cost * level_actions[t][i];
		}
		model.setObjective(obj, GRB_MINIMIZE);


		for (Depth t = 0; t <= depth; ++t)
			for (Var i = 0; i < n_vars; ++i)
				if(!vars[i].is_axiom()){
					for (Val j = 0; j < vars[i].range; ++j) {
						Prop p(i, j);
						auto scv = level_env[t][p];
						auto sat = level_sat[t][p];

						/* sat -> add or preadd or maintain */
						model.addConstr(
							sat <= scv->add + scv->preadd + scv->maintain,
							"satImplysSCV_i" + to_string(i) + "_j" + to_string(j) +
								"_t" + to_string(t));

						/* add or preadd or maintain -> sat */
						model.addConstr(sat >= scv->add,
										"addImplysSat_i" + to_string(i) + "_j" +
											to_string(j) + "_t" + to_string(t));
						model.addConstr(sat >= scv->preadd,
										"preaddImplysSat_i" + to_string(i) + "_j" +
											to_string(j) + "_t" + to_string(t));
						model.addConstr(sat >= scv->maintain,
										"maintainImplysSat_i" + to_string(i) +
											"_j" + to_string(j) + "_t" +
											to_string(t));
					}
				}

		int c_index = 0;

		int idx = 0;
		for (auto s : prob->init) {
			for (Val val = 0; val < vars[idx].range; ++val) {
				Prop p(idx, val);
				if(!vars[idx].is_axiom())
				{
					GrbSCV scv = level_env[0][p];
					if (s == val)
						model.addConstr(scv->add == 1, "c2_" + to_string(c_index));
					else {
						model.addConstr(scv->add == 0,
										"c3_add_" + to_string(c_index));
						model.addConstr(scv->maintain == 0,
										"c3_maintain_" + to_string(c_index));
						model.addConstr(scv->preadd == 0,
										"c3_preadd_" + to_string(c_index));
					}
				}
				++c_index;
			}
			++idx;
		}

		c_index = 0;
		for (auto p : prob->goal) {
			model.addConstr(level_sat[depth][p]>= 1,
			                "c4_" + to_string(c_index));
			++c_index;
		}

		c_index = 0;
		for (Depth t = 1; t <= depth; ++t) {
			GrbEnv this_env = level_env[t];
			GrbActions act = level_actions[t - 1];
			for (auto e : this_env) { 
				auto p = e.first;
				auto scv = *e.second;

				GRBLinExpr rhs;
				/* XXX: these routine in this loop may be innefficient */
				ASet *add_sub_pref = create_diff(prob->addf.at(p), prob->pref.at(p));
				ASet *del_sub_pref = create_diff(prob->delf.at(p),prob->pref.at(p));
				ASet *pre_sub_delf = create_diff(prob->pref.at(p), prob->delf.at(p));
				ASet *pre_prod_delf = create_prod(prob->delf.at(p), prob->pref.at(p));

				rhs = 0.0;
				for (auto op : *add_sub_pref) {
					rhs += act[op];
					model.addConstr(scv.add >= act[op],
					                "c6_" + to_string(c_index));
					++c_index;
				}
				model.addConstr(scv.add <= rhs, "c5_" + to_string(c_index));

				rhs = 0.0;
				for (auto op : *del_sub_pref) {
					rhs += act[op];
					model.addConstr(scv.del >= act[op],
					                "c8_" + to_string(c_index));
					++c_index;
				}
				model.addConstr(scv.del <= rhs, "c7_" + to_string(c_index));

				rhs = 0.0;
				for (auto op : *pre_sub_delf) {
					rhs += act[op];
					model.addConstr(scv.preadd >= act[op],
					                "c10_" + to_string(c_index));
					++c_index;
				}
				/* XXX: c9 exist in lp file ? */
				model.addConstr(scv.preadd <= rhs, "c9_" + to_string(c_index));

				rhs = 0.0;
				for (auto op : *pre_prod_delf)
					rhs += act[op];
				model.addConstr(scv.predel == rhs, "c11_" + to_string(c_index));

				/* counting constraint
				 * I(p) + add(p) - predel(p) >= G(p)
				 */
				/*
				 * rhs = 0.0;
				 * rhs += level_env[0][p] - level_env[depth][p]
				 * for(auto op : *pre_prod_delf)
				 * rhs -= act[op];
				 */

				delete add_sub_pref;
				delete del_sub_pref;
				delete pre_sub_delf;
				delete pre_prod_delf;
				++c_index;
			}

			for (Var i = 0; i < n_vars; ++i){
				if(vars[i].is_axiom()){
					for (unsigned int j = 0; j < vars.at(i).range; ++j) {
						Prop p(i,j);
						ASet *pre_sub_delf = create_diff(prob->pref.at(p), prob->delf.at(p));
						for (auto op : *pre_sub_delf) {
							model.addConstr( level_sat[t-1][p] >= act[op],
											"c10'_" + to_string(c_index));
							++c_index;
						}
						delete pre_sub_delf;
					}
				}
			}
		}

		c_index = 0;
		for (Var i = 0; i < n_vars; ++i)
			if(!vars[i].is_axiom()){
				for (Val j = 0; j < vars.at(i).range; ++j) {
					Prop p(i, j);
					GRBLinExpr lhs, rhs = 0;

					int counter = 0;
					for (auto this_env : level_env) {
						auto scv = this_env[p];
						model.addConstr(
							scv->add + scv->maintain + scv->del + scv->predel <= 1,
							"c12_" + to_string(c_index));
						model.addConstr(
							scv->preadd + scv->maintain + scv->del + scv->predel <=
								1,
							"c13_" + to_string(c_index));

						lhs = 0.0;
						lhs += scv->preadd + scv->maintain + scv->predel;  // t
						if (counter > 0)
							model.addConstr(lhs <= rhs,
											"c14_" + to_string(c_index));
						rhs = 0.0;
						rhs += scv->preadd + scv->add + scv->maintain;  // t-1

						++counter;
						++c_index;
					}
				}
			}

		if(n_axioms > 0){
			assert(config.use_among);
			int a_index = 0;
			int b_index=0;
			int c_index=0;

			c_index = 0;
			for (Depth t = 1; t <= depth; ++t) {
				GRBLinExpr sum = 0.0;
				for(unsigned int i = 0; i < n_ops;++i){
					sum += level_actions[t-1][i];
				}
				model.addConstr(sum <= 1, "c16_" + to_string(c_index));
				++c_index;
			}

			for (Depth t = 0; t <= depth; ++t) {
				for(unsigned int i = 0; i < n_axioms; ++i){
					auto axiom = prob->axioms.at(i);
					auto head = axiom.effect;
					unsigned int n_conditions = axiom.n_conditions;

					if(n_conditions > 0){
						GRBLinExpr b_sum=0;
						GRBLinExpr c_sum=0;
						int n_b=0;
						int n_c=0;
						for(unsigned int j = 0; j < n_conditions; ++j){
							auto prop = axiom.conditions[j];
							auto var = vars[prop.first];
							if(var.is_axiom() && prop.second == 0){
								prop.second = 1;
								c_sum += level_sat[t][prop];
								++n_c;
							}else{
								b_sum += level_sat[t][prop];
								++n_b;
							}
						}
						model.addConstr(
							b_sum - c_sum - level_axioms[t][i] <= n_b  -1,
							"a10_" + to_string(a_index));
						model.addConstr(
							b_sum - c_sum - level_axioms[t][i]*n_conditions >= - n_c,
							"a9_" + to_string(a_index));

						for(auto index : prob->lnt.at(head)){
							auto internal_rule = prob->axioms.at(index);
							vector<Prop> is;
							set_intersection(internal_rule.conditions.begin(),internal_rule.conditions.end(),
									prob->scc.at(head).begin(),prob->scc.at(head).end(),back_inserter(is));

							model.addConstr(
								level_axioms[t][index] - level_ss[t][index] >= 0,
								"a12_" + to_string(a_index));

							GRBLinExpr gt_sum=0;
							for(auto b : is){
								auto pp = pair<int,int>(head.first,b.first);
								gt_sum += level_gts[t][pp];
								model.addConstr( level_ranks[t][head.first] - level_ranks[t][b.first] 
										- prob->scc.at(head).size()* level_gts[t][pp] +prob->scc.at(head).size()>= 1 ,
									"a14_" + to_string(c_index) );
								c_index++;
							}
							model.addConstr(
								gt_sum - is.size()*level_ss[t][index] >= 0,
								"a13_" + to_string(b_index) );
								b_index++;
						}
					}
					a_index++;
				}
			}

			a_index=0;
			b_index=0;
			for(Depth t = 0; t <= depth; ++t){
				for (Var i = 0; i < n_vars; ++i){
					if(vars[i].is_axiom()){
						Prop pos(i, 1);
						Prop neg(i, 0);
						GRBLinExpr def_sum=0;
						GRBLinExpr ext_sum=0;
						GRBLinExpr lnt_sum=0;
						int n_def=0;
						for(auto index : prob->def.at(pos)){
							auto axiom = prob->axioms.at(index);
							def_sum += level_axioms[t][index];
							++n_def;
						}
						for(auto index : prob->ext.at(pos)){
							auto axiom = prob->axioms.at(index);
							ext_sum += level_axioms[t][index];
						}
						for(auto index : prob->lnt.at(pos)){
							lnt_sum += level_ss[t][index];
						}
						model.addConstr(
							def_sum - n_def * level_sat[t][pos] <=0,
							"a8_" + to_string(a_index));
						model.addConstr(
							ext_sum + lnt_sum- level_sat[t][pos] >= 0,
							"a11_" + to_string(a_index));
						++a_index;
					}
				}
			}
		}

		if(config.para_level == Sequential){
			int c_index=0;

			c_index = 0;
			for (Depth t = 1; t <= depth; ++t) {
				GRBLinExpr sum = 0.0;
				for(unsigned int i = 0; i < n_ops;++i){
					sum += level_actions[t-1][i];
				}
				model.addConstr(sum <= 1, "c16_" + to_string(c_index));
				++c_index;
			}
		}

		/* NOTE: depth=0 is included in c2,c3 (init condition constraint) */
		if (config.use_simple_pg) {
			c_index = 0;
			for (Depth t = 1; t <= depth; ++t) {
				auto this_env = level_env[t];
				Var var_i = 0;
				for (auto var : prob->vars) {
					for (Val val = 0; val < var.range; ++val) {
						Prop p(var_i, val);
						auto scv = this_env[p];
						if (pg[p] == PG_UNACTIVATED || t < pg[p]) {
							model.addConstr(
							    scv->add == 0,
							    "pgraph_add0_" + to_string(c_index));
							model.addConstr(
							    scv->preadd == 0,
							    "pgraph_preadd0_" + to_string(c_index));
							model.addConstr(
							    scv->maintain == 0,
							    "pgraph_maintain0_" + to_string(c_index));
							++c_index;
						}
					}
					++var_i;
				}
			}
		}

		if (config.use_among) {
			GRBLinExpr lhs;
			for (Depth t = 0; t <= depth; ++t) {
				auto this_satenv = level_sat[t];

				c_index = 0;
				for (Var var = 0; var < n_vars; ++var) {
					lhs = 0;
					for (Val val = 0; val < prob->vars[var].range; ++val) {
						Prop p(var, val);
						lhs += this_satenv[p];
					}

					model.addConstr(lhs == 1, "among_var_t" + to_string(t) +
					                              "_" + to_string(c_index));
					++c_index;
				}

				c_index = 0;
				for (auto mutex : prob->mtxs) {
					lhs = 0;
					for (auto p : mutex.list)
						lhs += this_satenv[p];

					model.addConstr(lhs <= 1, "among_mutex_t" + to_string(t) +
					                              "_" + to_string(c_index));
					++c_index;
				}
			}
		}

		model.update();

//	if(config.depth <= 0){
//		start_timer();
//		model.write("tmp_ub.lp");
//		stop_timer("write lp file");
//	}

		start_timer();
		model.optimize();
		stop_timer("optimize");

		const int OPTIMAL = 2;
		if (OPTIMAL == model.get(GRB_IntAttr_Status)) {
			for (auto actions_t : level_actions)
				for (auto i : actions_t) {
					if (i.get(GRB_DoubleAttr_X) == 1) {
						cout << "(" << i.get(GRB_StringAttr_VarName) << ")"
						     << endl;
					}
				}

			bool save_plan = true;
			if (save_plan) {
				ofstream ofs("sas_plan");

				for (auto actions_t : level_actions)
					for (auto i : actions_t) {
						if (i.get(GRB_DoubleAttr_X) == 1) {
							ofs << "(" << i.get(GRB_StringAttr_VarName) << ")"
							    << endl;
						}
					}
			}

			cout << "objval: " << model.get(GRB_DoubleAttr_ObjVal) << endl;
		}
	} catch (GRBException e) {
		cout << "Error code: " << e.getErrorCode() << ", " << e.getMessage()
		     << endl;

		destroy_scv(level_env, depth);
		throw e;

	} 

	destroy_scv(level_env, depth);

	cout << "------------------" << endl;
	return model.get(GRB_IntAttr_Status);
}
