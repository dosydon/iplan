#pragma once

#include "sas.h"

#include <vector>
#include <map>
#include <set>

using std::vector;
using std::map;
using std::set;

using Depth = unsigned int;
using SCV = struct scv_data { bool maintain, add, preadd, del, predel; };

using Env = map<Prop, SCV>;
using LevelEnv = vector<Env>;
using Actions = vector<bool>;
using LevelActions = vector<Actions>;
ASet *create_prod(const ASet &lset, const ASet &rset);
ASet *create_diff(const ASet &lset, const ASet &rset);
void destroy_aset(ASet *s);
