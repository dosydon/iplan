import os
import errno
import resource
import signal
from signal_handlers import *

def get_elapsed_time():
    return sum(os.times()[:4])

def file_remove(filename):
    try:
        os.remove(filename)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise
        
def clean():
    file_remove("output.sas")
    file_remove("output")
    file_remove("output.lp")
    file_remove("output.mzn")
    file_remove("sas_plan")
    file_remove("clingo_plan")

def set_limits(args):
    signal.signal(signal.SIGALRM, sigalrm_handler)
    resource.setrlimit(resource.RLIMIT_AS,(args.mem_limit * 1024 * 1024,args.mem_limit * 1024 * 1024))
    signal.alarm(args.time_limit)

def common_opts(parser):
    parser.add_argument("dom_file")
    parser.add_argument("ins_file")
    parser.add_argument("--candidate_gen",default='opgraph')
    parser.add_argument("--axiom",help="enable axiom encoding and decoding",action="store_true")
    parser.add_argument("--smart",help="enable axiom encoding and decoding",action="store_true")
    parser.add_argument("--pg",help="use planning graph",action="store_true")
    parser.add_argument("--time_limit",help="depth",type=int,default=1800)
    parser.add_argument("--mem_limit",help="depth",type=int,default=2048)
