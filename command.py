import time
import subprocess
import psutil
import utils
import argparse
import os.path, sys
from log import *
from math import ceil

FILE_DIR = os.path.dirname(os.path.realpath(__file__))
PAR_DIR = os.path.realpath(os.path.join(FILE_DIR, os.pardir))

FD = os.path.join(PAR_DIR, 'translator-es/translate.py')
FDAXIOM = os.path.join(PAR_DIR, 'fd-axiom/src/fast-downward.py')
EXT_HOME = os.path.join(PAR_DIR, 'tau_axiom_extractor')
SAS2GUROBI = os.path.join(FILE_DIR ,'src/sas2gurobi/sas2gurobi')

class Command:
    def __init__(self,cmd,tag,**kwargs):
        self.cmd = cmd
        self.tag = tag
        self.kwargs = kwargs
    def proc(self,timeout=None,preexec_fn=None):
        return subprocess.Popen(self.cmd.split(' '),preexec_fn=preexec_fn,**self.kwargs)
    def __repr__(self):
        return (self.cmd)

class TranslateCmd(Command):
    def __init__(self,args):
        if hasattr(args, 'translate_args'):
            cmd = "{} {} {} {}".format(FD,args.dom_file,args.ins_file,args.translate_args)
        else:
            cmd = "{} {} {}".format(FD,args.dom_file,args.ins_file)
        tag = Tag.TRANSLATE
        super().__init__(cmd,tag)

class EncodeCmd(Command):
    def __init__(self,args):
        cmd = "{}/encode.py output.sas --candidate_gen {}".format(EXT_HOME,args.candidate_gen)
        tag = Tag.ENCODE
        super().__init__(cmd,tag)

class DecodeCmd(Command):
    def __init__(self,args):
        cmd ="{}/decode.py output.sas sas_plan".format(EXT_HOME) 
        tag = Tag.DECODE
        super().__init__(cmd,tag)

class PreprocessCmd(Command):
    def __init__(self,args):
        cmd = "{}/preprocess.py output.sas".format(EXT_HOME)
        tag = Tag.PREPROCESS
        super().__init__(cmd,tag)

class Sas2GurobiCmd(Command):
    def __init__(self,args,**kwargs):
        cmd = "{} -i output.sas -o {} -m {} -l {} -p {:d} -a {:d} -d {:d}".format(SAS2GUROBI,args.output,args.opt_level,args.para_level,args.pg,args.among,args.depth)
        tag = Tag.HP
        super().__init__(cmd,tag,**kwargs)
