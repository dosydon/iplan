#pragma once

#include <map>
#include <vector>
#include "gurobi_c++.h"
#include "model.h"

#define ADDBINARY(model, name) (model).addVar(0.0, 1.0, 0.0, GRB_BINARY, (name))
#define ADDBINARY_WITH_COST(model, name, obj) \
	(model).addVar(0.0, 1.0, (obj), GRB_BINARY, (name))


typedef struct StateChangeVariable {
	GRBVar add, del, preadd, predel, maintain;
} * GrbSCV;

typedef std::map<Prop, GrbSCV> GrbEnv;
typedef std::vector<GrbEnv> GrbLevelEnv;

typedef std::map<Prop, GRBVar> GrbSat;
typedef std::vector<GrbSat> GrbLevelSat;

typedef std::vector<GRBVar> GrbActions;
typedef std::vector<GrbActions> GrbLevelActions;

typedef std::vector<GRBVar> GrbCondEff;
typedef std::vector<GrbCondEff> GrbCondEffs;
typedef std::vector<GrbCondEffs> GrbLevelCondEffs;

typedef std::vector<std::map<Prop, GRBVar>> Cause;
typedef std::map<Prop, std::map<Prop, GRBVar>> CausalEdge;

typedef vector<GRBVar> GrbAxioms;
typedef vector<GrbAxioms> GrbLevelAxioms;

typedef map<Var, GRBVar> GrbRank;
typedef vector<GrbRank> GrbLevelRanks;

typedef vector<GRBVar> GrbS;
typedef vector<GrbS> GrbLevelSS;

typedef map<pair<Var,Var>, GRBVar> GrbGT;
typedef vector<GrbGT> GrbLevelGTS;

GrbSCV create_scv(GRBModel &model, const std::string &prop_name) ;
void destroy_scv(GrbLevelEnv level_env, Depth depth) ;
