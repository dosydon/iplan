#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import subprocess
import time
import functools
import psutil
import json
import os
import errno
import resource
from log import *

import utils
import itertools
import os
from executor import Executor
from command import *
def gurobi_main(args,log):
    ex = Executor(log,args.time_limit)

    args.depth = 0
    removed = False
    utils.clean()
    ex.run(TranslateCmd(args))
    ex.run(PreprocessCmd(args))

    if args.axiom:
        ex.run(EncodeCmd(args))
        with open('output.sas','r') as f:
            if 'removed_operator' in f.read():
                removed = True
        if (not removed) and args.smart and hasattr(args, 'translate_args'):
            del args.translate_args
            ex.run(TranslateCmd(args))

    ex.run(Sas2GurobiCmd(args))

    if removed and os.path.isfile("sas_plan"):
        ex.run(DecodeCmd(args))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    utils.common_opts(parser)
    parser.add_argument("--among",help="enable among constraints",action="store_true")
    parser.add_argument("--single_thread",help="single_thread(gurobi)",action="store_true")
    parser.add_argument("--opt_level",help="specify optimization level(satisficing|step|global)",default="satisficing")
    parser.add_argument("--para_level",help="specify para_level(sequential|all_step)",default="all_step")
    parser.add_argument("--depth",help="depth",type=int,default=0)

    subparsers = parser.add_subparsers(help='sub-command help')
    translate_parser = subparsers.add_parser('translate',help=' help')
    translate_parser.add_argument('translate_args',help='translate args',type=str,default=' ')

    args = parser.parse_args()
    print(args)
    utils.set_limits(args)
    
    log = []

    try:
        args.output = 'output.lp'
        gurobi_main(args,log)
    except Exception as e:
        raise e
    finally:
        verbose_log(log)
        aggregate_log(log)
        print("MAXRSS:{:d}".format(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss + resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss))

