#include "gurobi_var.h"

GrbSCV
create_scv(GRBModel &model, const std::string &prop_name) {
	GrbSCV v = new struct StateChangeVariable;

	v->maintain = ADDBINARY(model, prop_name + "_maintain");
	v->add = ADDBINARY(model, prop_name + "_add");
	v->preadd = ADDBINARY(model, prop_name + "_preadd");
	v->del = ADDBINARY(model, prop_name + "_del");
	v->predel = ADDBINARY(model, prop_name + "_predel");

	return v;
}

void
destroy_scv(GrbLevelEnv level_env, Depth depth) {
	for (Depth t = 0; t < depth; ++t)
		for (auto scv : level_env[t])
			delete scv.second;
}
