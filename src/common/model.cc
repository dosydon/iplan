#include <algorithm>
#include <limits.h>

#include "model.h"

void
destroy_aset(ASet *s) {
	delete s;
}

ASet *
create_prod(const ASet &lset, const ASet &rset) {
	ASet *ans = new ASet();
	set_intersection(lset.begin(), lset.end(), rset.begin(), rset.end(),
	                 inserter(*ans, ans->begin()));
	return ans;
}

ASet *
create_diff(const ASet &lset, const ASet &rset) {
	ASet *ans = new ASet();
	set_difference(lset.begin(), lset.end(), rset.begin(), rset.end(),
	               inserter(*ans, ans->begin()));
	return ans;
}

