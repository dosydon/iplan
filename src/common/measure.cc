#include <chrono>
#include <iostream>

#include "measure.h"

using namespace std;

static chrono::time_point<chrono::system_clock> start_time, end_time;

void
start_timer(void) {
	cout << "timer started." << endl;
	start_time = chrono::system_clock::now();
}

void
stop_timer(string label) {
	end_time = chrono::system_clock::now();
	auto timespan = end_time - start_time;
	cout << "time(" << label << "):"
	     << chrono::duration_cast<chrono::milliseconds>(timespan).count()
	     << "[ms]" << endl;
}
